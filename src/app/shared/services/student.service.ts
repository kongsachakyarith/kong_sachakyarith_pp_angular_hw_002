import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Istudent } from 'src/app/model/istudent';

@Injectable({
  providedIn: 'root'
})
export class StudentService {

  //create observable
  studentObs$ !: Observable<Istudent[]>;

  student:Istudent[] =[
    {
      name: "yarith",
      gender: "Male",
      subject: "DevOps"
    },
    {
      name: "yarith",
      gender: "Male",
      subject: "Data Analytics"
    },
    {
      name: "yarith",
      gender: "Male",
      subject: "Spring"
    },
    {
      name: "dara",
      gender: "Male",
      subject: "Android"
    },
     {
      name: "dara",
      gender: "Male",
      subject: "Blockchain"
    },
    {
      name: "Kimhab",
      gender: "Male",
      subject: "iOS"
    },
  ]

  addStudent(students:any):void{
    this.student.push(students);
    console.log(this.student);
  }

  constructor() { 
    this.studentObs$ = of(this.student)
  }

  getstu():Observable<Istudent[]>{
    return this.studentObs$;
  }
  getdata(data:any){
    this.student = data;
  }
}
