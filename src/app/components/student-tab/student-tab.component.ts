import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Istudent } from 'src/app/model/istudent';
import { StudentService } from 'src/app/shared/services/student.service';

@Component({
  selector: 'app-student-tab',
  templateUrl: './student-tab.component.html',
  styleUrls: ['./student-tab.component.css'],
})
export class StudentTabComponent implements OnInit {
  student_table!: Istudent[];
  selectedType: string = '';
  constructor(private _ddf: StudentService) {}

  ngOnInit(): void {
    this._ddf.getstu().subscribe((students: Istudent[]) => {
      this.student_table = students;
    });
  }
  getAllData() {
    this._ddf.getstu().subscribe((value: Istudent[]) => {
      this.student_table = value;
    });
  }
  deleteEmployee(i: any) {
    this.student_table.splice(i, 1);
  }
  select(type: any) {
    this.student_table = [];
    this._ddf.getstu().subscribe((value: Istudent[]) => {
      let data = [];
      data = value;
      console.log(data);
      for (let i = 0; i < data.length; i++) {
        if (data[i].subject === type) {
          this.student_table.push(data[i]);
        }
      }
    });
  }
}
