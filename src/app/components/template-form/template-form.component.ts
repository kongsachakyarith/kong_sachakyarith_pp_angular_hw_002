import { Component, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Istudent } from 'src/app/model/istudent';
import { StudentService } from 'src/app/shared/services/student.service';

@Component({
  selector: 'app-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.css'],
})
export class TemplateFormComponent implements OnInit {
  studentForm!: FormGroup;
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private _studentService: StudentService
  ) {
    

    this.studentForm = this.fb.group({
      name: ['', 
        Validators.required,
      ],
      gender: ['', Validators.required],
      subject: ['', Validators.required],
    });
  }

  ngOnInit(): void {
  }
  element = true;

  showData() {
    return (this.element = true);
  }
  hideData() {
    return (this.element = false);
  }
  get f(): { [key: string]: AbstractControl } {
    return this.studentForm.controls;
  }

  
  onSubmit():void{
    this.submitted = true;

    if (this.studentForm.invalid) {
      return;
    }
    const student={
      name:this.studentForm.value.name,
      gender:this.studentForm.value.gender,
      subject:this.studentForm.value.subject
    }
    this._studentService.addStudent(student);
  }
}
