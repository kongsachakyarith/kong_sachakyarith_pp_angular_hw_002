import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title = 'angular-form';

  student={
    name: "Kimhab",
    gender: "Male",
    subject: "ios"
  }
  getdata(data:any){
    this.student = data;
  }


}
